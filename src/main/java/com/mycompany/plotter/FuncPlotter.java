package com.mycompany.plotter;

import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Random;

public class FuncPlotter {
    private JTextField textField1;
    private JButton drawButton;
    private JButton clearAllButton;
    private JPanel jPanel1;
    private JPanel rootPanel;

    public FuncPlotter() {
        drawButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
                super.mouseClicked(mouseEvent);

                drawXYAxis(); // Draws coordinate plane

                Graphics2D g = (Graphics2D) jPanel1.getGraphics();
                g.translate(200, 200); // New plain orgin

                Random rand = new Random();
                int x1 = rand.nextInt(255);
                int x2 = rand.nextInt(255);
                int x3 = rand.nextInt(255);
                g.setColor(new Color(x1, x2, x3)); // Random color
                Expression exp;
                try {
                     exp = new ExpressionBuilder(textField1.getText())
                            .variables("x")
                            .build();
                } catch (Exception e) {
                    badFormula();
                    return;
                }

                exp.setVariable("x", 1);
                Polygon p = new Polygon();
                for (int x = -200; x <= 200; ++x) {
                    Expression expAtPoint = exp.setVariable("x", x);
                    if (expAtPoint.validate().isValid()) {
                        p.addPoint(x, - (int) expAtPoint.evaluate());
                    } else {
                        badFormula();
                        return;
                    }
                }
                g.drawPolyline(p.xpoints, p.ypoints, p.npoints);
            }
        });
        clearAllButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
                super.mouseClicked(mouseEvent);
                jPanel1.repaint();
            }
        });
    }

    private void drawXYAxis()
    {
        Graphics g = jPanel1.getGraphics();
        g.translate(200, 200); // New plane orgin
        g.setColor(new Color(0, 0, 0)); // Black color
        g.drawLine(-200, 0, 200, 0); // X axis
        g.drawString("X", 180, 20); // Under X axis
        g.drawLine(0, 200, 0, -200); // Y axis
        g.drawString("Y", 10, -180); // Next to Y axis
        g.drawLine(100, 5, 100, -5); // On X axis right
        g.drawString("1", 98, 20); // Under X axis right
        g.drawLine(-100, 5, -100, -5); // On X axis left
        g.drawString("-1", -105, 20); // Under X axis left
        g.drawLine(5, -100, -5, -100); // On Y axis top
        g.drawString("1", 10, -95); // Next to Y axis top
        g.drawLine(5, 100, -5, 100); // On Y axis bottom
        g.drawString("-1", 10, 105); // Next to Y axis bottom
    }

    private static void badFormula()
    {
        JOptionPane.showMessageDialog(null, "Bad formula", "Error", JOptionPane.INFORMATION_MESSAGE);
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("Plotter");
        frame.setContentPane(new FuncPlotter().rootPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
