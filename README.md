

Program służący do rysowania wykresów funkcji.

Klasa uruchamiająca program -- FuncPlotter.java


Spełnione wymagania funkcjonalne:
1. Program rysuje wykres dowolnej funckji składającej się z wyrażeń arytmetucznych lub funkcji log, cos, sin.
2. Program umożliwia tworzenie wielu wykresów na jednym rysunku.
3. Program zawiera opcję wyczyszczenia rysunku

Spełnione wymagania niefunkcjonalne:
1. Wykresy są łatwe do odróżnienia
2. Komunikaty o błędach
3. Program oparty na Swing framework